FROM openjdk:jre-slim
MAINTAINER github.com/rubenqba
RUN apt-get update && apt-get install -y --no-install-recommends \
		curl \
	&& rm -rf /var/lib/apt/lists/*
